package com.backend.maelsa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaelsaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MaelsaApplication.class, args);
	}

}
