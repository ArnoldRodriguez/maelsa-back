/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.maelsa.controller;

import com.backend.maelsa.model.Machine;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author black
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class MachineController {

    @GetMapping(value = "/create/machine")
    public Machine createMachine() {
        Machine machine = new Machine();
        machine.setName("Selladora 1");
        machine.setDescription("Maquina Selladora");
        machine.setPrecio(1234.56);
        return machine;
    }
    
    @GetMapping(value = "/title")
    public Map generateTitle() {
        Map map = new HashMap();
        map.put("title", "TITILOOOOOOOOOOOOO");
        map.put("descriptionm", "fffff");
        map.put("subtitulo", "aswdasads");
        return map;
    }
    @GetMapping(value = "/machines")
    public List<Machine> getAllMachines(){
        List<Machine> machines = new ArrayList<>();
        Machine machine = new Machine("GHJ", "Maquina 1", 1234.2342);
        Machine machine1 = new Machine("vdfvd", "Maquina 2", 1234.2342);
        Machine machine2 = new Machine("2131s", "Maquina 3", 123454.9);
        Machine machine3 = new Machine("aqw2", "Maquina 4", 3123123);
        Machine machine4 = new Machine("12saq", "Maquina 5", 87654.87);
        machines.add(machine);
        machines.add(machine1);
        machines.add(machine2);
        machines.add(machine3);
        machines.add(machine4);
        return machines;
    }
    
    @GetMapping(value = "/names")
    public List<String> getAllNames(){
        List<String> names = new ArrayList<>();
        String name1 = "nsme1";
        String name2 = "nsme1";
        names.add(name1);
        names.add(name2);
        
        return names;
        
    }
}
