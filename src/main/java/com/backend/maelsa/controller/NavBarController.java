/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.maelsa.controller;

import com.backend.maelsa.model.Machine;
import com.backend.maelsa.model.NavBarItem;
import com.backend.maelsa.model.Response;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author black
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class NavBarController {
 
    @GetMapping(value = "/itemsnavba")
    public Response listNavBar() {
        List<NavBarItem> navbar = new ArrayList<>();
        NavBarItem itemMachine = new NavBarItem("Machine","/machines");
        NavBarItem cabello = new NavBarItem("Cabello","/cabello");
        NavBarItem infoUser = new NavBarItem("Informacion","/infouser1");
        NavBarItem animal =new NavBarItem("Animal","animal");
        navbar.add(cabello);
        navbar.add(itemMachine);
        navbar.add(infoUser);
        navbar.add(animal);
        Response response = new Response();
        response.setCode("200");
        response.setMsg("Items cargados");
        response.setList(navbar);
        return response;
    }
}
