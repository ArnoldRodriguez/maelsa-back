/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.maelsa.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author black
 */
@RestController
public class IngresoContrtoller {
 
    @GetMapping(value="/ingresar/{bloque}/{casa}")
    public String Ingreso(@PathVariable String bloque, @PathVariable String casa) {
        
        String msg = "Ha ingresado al bloque : " +bloque +" casa: "+ casa;
        System.out.println(msg);
        return msg;
    }
    @GetMapping(value="/lavar/{bloque}/{casa}")
    public String LavarVentana(@PathVariable String bloque, @PathVariable String casa, @RequestParam String ventana) {
        
        String msg = "Ha ingresado al bloque : " +bloque +" casa:  "+ casa + " Ventana" + ventana;
        System.out.println(msg);
        return msg;
    }
}
