/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.maelsa.controller;

import com.backend.maelsa.model.Cabello;
import com.backend.maelsa.model.Response;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author black
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ControllerCabello {
    @GetMapping(value="/cabello")
    public Response User1(){
        System.out.println("entro");
        Cabello cabello =new Cabello();
        cabello.setColor("Rojo");
        cabello.setForma("Liso");
        cabello.setTamaño("Corto");
        Response response = new Response();
        response.setMsg("satisfactorio");
        response.setCode("200");
        response.setObject(cabello);
        return response;
    }
}
