/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.maelsa.controller;

import com.backend.maelsa.model.Animal;
import com.backend.maelsa.model.Response;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author black
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AnimalController {

    @GetMapping(value = "animal/{name}")
    public Response animal(@PathVariable String name) {
        System.out.println("llego");
        Animal animal = new Animal();
        Response response = new Response();
        if (name.equals("perro")) {
            animal.setNombre("Perro");
            animal.setColor("Cafe");
            animal.setSonido("Guaguaua");
            animal.setTipo("Canino");

        } else if (name.equals("gato")) {
            animal.setNombre("Gato");
            animal.setColor("blanco");
            animal.setSonido("Miaiaiauuau");
            animal.setTipo("Felino");
        } else {
            animal.setColor("No existe");
            animal.setColor("No existe");
            animal.setSonido("No existe");
            animal.setTipo("No existe");
        }

        response.setMsg("satisfactorio");
        response.setCode("200");
        response.setObject(animal);
        return response;
    }
}
