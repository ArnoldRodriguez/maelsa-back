/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.maelsa.controller;

import com.backend.maelsa.model.Response;
import com.backend.maelsa.model.UserInfo;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author black
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserController {

    @GetMapping(value="/create/user/{name}")
    public String Create(@PathVariable String name, @RequestParam String pass, @RequestParam String lastName) {
        
        String msg = "Ha creado un usuario con name : " +name +" con passss-:"+ pass + "  ladstnmae" + lastName;
        System.out.println(msg);
        return msg;
    }
    @GetMapping(value="/deleate/user/{elimuser}/{passw}")
    public void Deleate(@PathVariable String elimuser, @PathVariable String passw) {
        String out = "Ha eliminado el usuario: "+elimuser+"con clave: "+passw;
        System.out.println(out);
    }
    @GetMapping(value="/auth/user")
    public void Authent(@RequestParam String nombre,@RequestParam String clave) {
        String sal= "Se ha autenticado con nombre : "+nombre+ " y con clave: "+clave;
        System.out.println(sal);
    }
    @GetMapping(value="/deslog/user/{nombe}")
    public void Deslog(@PathVariable String nombe, @RequestParam String clav) {
        String ous= "Se ha deslogueado con no mbre: "+ nombe + " con clave: "+clav;
        System.out.println(ous);
    }
    @GetMapping(value="/user")
    public UserInfo User(){
        UserInfo userinfo=new UserInfo();
        userinfo.setId("12334");
        userinfo.setName("assssda");
        return userinfo;
    }
    
    
    @GetMapping(value="/user1")
    public Response User1(){
        UserInfo userinfo=new UserInfo();
        userinfo.setId("12334");
        userinfo.setName("assssda");
        Response response = new Response();
        response.setMsg("satisfactorio");
        response.setCode("200");
        response.setObject(userinfo);
        return response;
    }

}
