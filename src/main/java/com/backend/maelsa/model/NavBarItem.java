/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.maelsa.model;

/**
 *
 * @author black
 */
public class NavBarItem {
    private String title;
    private String path;

    public NavBarItem() {
    }

    public NavBarItem(String title, String path) {
        this.title = title;
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "NavBarItems{" + "title=" + title + ", path=" + path + '}';
    }
    
    
}
