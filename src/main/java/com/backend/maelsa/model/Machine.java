/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.maelsa.model;

/**
 *
 * @author black
 */
public class Machine {

    private String name;
    private String description;
    private double precio;

    public Machine() {
    }

    public Machine(String name, String description, double precio) {
        this.name = name;
        this.description = description;
        this.precio = precio;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Machine{" + "name=" + name + ", description=" + description + ", precio=" + precio + '}';
    }

}
