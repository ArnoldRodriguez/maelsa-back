/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.maelsa.model;

import static org.apache.tomcat.jni.Lock.name;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author black
 */

public class Animal {
    
   private String nombre;
   private String tipo;
   private String color;
   private String sonido;

    public Animal() {
    }

    public Animal(String nombre, String tipo, String color, String sonido) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.color = color;
        this.sonido = sonido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSonido() {
        return sonido;
    }

    public void setSonido(String sonido) {
        this.sonido = sonido;
    }

    @Override
    public String toString() {
        return "Animal{" + "nombre=" + nombre + ", tipo=" + tipo + ", color=" + color + ", sonido=" + sonido + '}';
    }
   
}
