/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.maelsa.model;

/**
 *
 * @author black
 */
public class Cabello {
    
    private String color;
    private String tamaño;
    private String forma;

    public Cabello() {
    }

    public Cabello(String color, String tamaño, String forma) {
        this.color = color;
        this.tamaño = tamaño;
        this.forma = forma;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }

    public String getForma() {
        return forma;
    }

    public void setForma(String forma) {
        this.forma = forma;
    }

    @Override
    public String toString() {
        return "Cabello{" + "color=" + color + ", tama\u00f1o=" + tamaño + ", forma=" + forma + '}';
    }
    
    
    
}
