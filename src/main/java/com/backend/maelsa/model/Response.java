/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.backend.maelsa.model;

import java.util.List;

/**
 *
 * @author black
 */
public class Response {
    
    private String msg;
    private String code;
    private Object object;
    private List list;

    public Response() {
        
    }

    public Response(String msg, String code, Object object) {
        this.msg = msg;
        this.code = code;
        this.object = object;
    }

    public Response(String msg, String code, Object object, List list) {
        this.msg = msg;
        this.code = code;
        this.object = object;
        this.list = list;
    }
    

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }
    
    

    
    
}
